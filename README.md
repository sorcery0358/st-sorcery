# st-sorcery
a custom build of suckless's st (simple terminal implementation for X) <br>
it is suitable with the colors of default gnome terminal color scheme
###
## recommended to use it with these custom builds of suckless software: 
dmenu-sorcery; https://gitlab.com/sorcery0358/dmenu-sorcery <br>
dwm-sorcery; https://gitlab.com/sorcery0358/dwm-sorcery <br>
slock-sorcery; https://gitlab.com/sorcery0358/slock-sorcery
## patches
this custom build of st includes these patches:
###
alpha; https://st.suckless.org/patches/alpha <br>
autocomplete;  https://st.suckless.org/patches/autocomplete <br>
blinking cursor;  https://st.suckless.org/patches/blinking_cursor <br>
boxdraw; https://st.suckless.org/patches/boxdraw <br>
dynamic-cursor-color; https://st.suckless.org/patches/dynamic-cursor-color <br>
scrollback; https://st.suckless.org/patches/scrollback
## installation
	$ cd st-sorcery/st-0.8.4
	$ sudo make clean install
## fetch-master-6000
this custom build includes a fetch tool by anhsirk0 <br>
fetch-master-6000; https://github.com/anhsirk0/fetch-master-6000
###
to activate it run these commands;
###
	$ cd st-sorcery/st-0.8.4/fetch-master-6000
	$ chmod +x fm6000
	$ sudo cp fm6000 /usr/bin
###
and add this into your shell config
###
	fm6000
## customization
to customize edit `st-sorcery/st-0.8.4/config.def.h` and run these commands;
###
	$ sudo cp config.def.h config.h
	$ sudo make clean install
##
`st-sorcery/st-0.8.4.tar.gz` is the vanilla version of st
